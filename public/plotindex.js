var ready = (callback) => {
  if (document.readyState != "loading") callback();
  else document.addEventListener("DOMContentLoaded", callback);
}

var myselect = document.getElementById("myselect").addEventListener('change',function(){
  var stock_val = this.value;
  var stock_text = this.options[this.selectedIndex].text;
  makeplot(stock_val,stock_text)
  
  function makeplot(stock_val,stock_text) {
    console.log("makeplot: start")
    fetch("data/"+stock_val+".csv")
    .then((response) => response.text()) /* asynchronous */
    .catch(error => {
        alert(error)
         })
    .then((text) => {
      console.log("csv: start")
      csv().fromString(text).then(processData)  /* asynchronous */
      console.log("csv: end")
    })
    console.log("makeplot: end")
  };
  
  function processData(data) {
  console.log("processData: start")
  let x = [], y = []
  
  for (let i=0; i<data.length; i++) {
      row = data[i];
      x.push( row['Date'] );
      y.push( row['Close'] );
  } 
  makePlotly( x, y );
  console.log("processData: end")
  }
  
  function makePlotly( x, y ){
  console.log("makePlotly: start")
  var traces = [{
        x: x,
        y: y
  }];
  var layout  = { title: stock_text+" Stock Price History"}
  
  myDiv = document.getElementById('myDiv');
  Plotly.newPlot( myDiv, traces, layout );
  console.log("makePlotly: end")
  };
});
